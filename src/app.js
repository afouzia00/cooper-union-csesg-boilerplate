/*
   --------
   import the packages we need
   --------
 */

import React from 'react';
import { connect, Provider } from 'react-redux';
import { createStore, combineReducers, compose } from 'redux';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './style/theme';
import initialState from './initialState.json';
import './style/main.css';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'




/*
   --------
   import your pages here
   --------
 */

import HomePage from './pages/home';
import LoginPage from './pages/login';
import SignupPage from './pages/signup';
import ExplorePage from './pages/explore';
import AboutPage from './pages/about';
import MyTimelines from './pages/myTimelines';
import signInteractives from './pages/signInteractives';
import profile from './pages/profile';




/*
   --------
   configure everything
   --------
 */

const firebaseConfig = {
    apiKey: "",
    authDomain: "machine-learning-career-plan.firebaseapp.com",
    databaseURL: "https://machine-learning-career-plan.firebaseio.com",
    projectId: "machine-learning-career-plan",
    storageBucket: "machine-learning-career-plan.appspot.com",
    messagingSenderId: "750481349557"
};

// react-redux-firebase config
const rrfConfig = {
    userProfile: 'users',
};

// Initialize firebase instance
firebase.initializeApp(firebaseConfig);









/*
   --------
   setup redux and router
   --------
 */


const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase, rrfConfig)
)(createStore);

// Add firebase to reducers
const rootReducer = combineReducers({
    firebase: firebaseReducer
});

const store = createStoreWithFirebase(rootReducer, initialState);


const ConnectedRouter = connect()(Router);



export default class App extends React.Component{
    render(){
	return(
	    <MuiThemeProvider theme={theme}>
		<Provider store={store}>
		    <ConnectedRouter>
			<div>
			    <Route exact path="/" component={HomePage} />
			    <Route exact path="/login" component={LoginPage} />
			    <Route exact path="/signup" component={SignupPage} />
          <Route exact path="/explore" component={ExplorePage}/>
          <Route exact path="/about" component={AboutPage}/>
          <Route exact path="/myTimelines" component={MyTimelines}/>
          <Route exact path="/signInteractives" component={signInteractives}/>
		  <Route exact path="/profile" component={profile}/>
		  <Route exact path="/HomePage" component={HomePage}/>
			</div>
		    </ConnectedRouter>
		</Provider>
	    </MuiThemeProvider>
	);
    }
}
