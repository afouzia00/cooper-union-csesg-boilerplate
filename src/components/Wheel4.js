import React from "react";

class Wheel_4 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }



  One() {

    this.props.callbackFromParent(this.props.ops.answers[0].a, this.props.ops.answers[0].next);
  }

  Two() {
        this.props.callbackFromParent(this.props.ops.answers[1].a, this.props.ops.answers[1].next,);
  }

  Three() {
        this.props.callbackFromParent(this.props.ops.answers[2].a, this.props.ops.answers[2].next,);
  }

  Four() {
        this.props.callbackFromParent(this.props.ops.answers[3].a, this.props.ops.answers[3].next,);
  }


  render() {
    console.log(this.props.ops)
    return (
      <div>
        <h1 id="HEADER">{this.props.ops.question}</h1>

                  <div id="menu">

        <div
            id="One"
            class="item1_4 item"
          onClick={() => {
            this.One();
          }}
        >
          <div class="content">
            <a>{this.props.ops.answers[0].a}</a>
          </div>
        </div>

        <div
            id="Two"
            class="item2_4 item"
          onClick={() => {
            this.Two();
          }}
        >
          <div class="content">
            <a>{this.props.ops.answers[1].a}</a>
          </div>
        </div>

        <div
            id="Three"
            class="item3_4 item"
          onClick={() => {
            this.Three();
          }}
        >
          <div class="content">
            <a>{this.props.ops.answers[2].a}</a>
          </div>
        </div>

        <div
            id="Four"
            class="item4_4 item"
          onClick={() => {
            this.Four();
          }}
        >
          <div class="content">
            <a>{this.props.ops.answers[3].a}</a>
          </div>
        </div>



        <div
          id="center"
        />
      </div>
              </div>
    );
  }
}

export default Wheel_4;
