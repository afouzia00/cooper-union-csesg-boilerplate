import React from "react";

class Wheel_5 extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  One() {
    this.props.callbackFromParent(
      this.props.ops.answers[0].a,
      this.props.ops.answers[0].next
    );
  }

  Two() {
    this.props.callbackFromParent(
      this.props.ops.answers[1].a,
      this.props.ops.answers[1].next
    );
  }

  Three() {
    this.props.callbackFromParent(
      this.props.ops.answers[2].a,
      this.props.ops.answers[2].next
    );
  }

  Four() {
    this.props.callbackFromParent(
      this.props.ops.answers[3].a,
      this.props.ops.answers[3].next
    );
  }

  Five() {
    this.props.callbackFromParent(
      this.props.ops.answers[4].a,
      this.props.ops.answers[4].next
    );
  }

  render() {

    return (
      <div>
        <h1 id="HEADER">{this.props.ops.question}</h1>

        <div id="menu">
          <div
            id="One"
            class="item1_5 item"
            onClick={() => {
              this.One();
            }}
          >
            <div class="content">
              <c>{this.props.ops.answers[0].a}</c>
            </div>
          </div>

          <div
            id="Two"
            class="item2_5 item"
            onClick={() => {
              this.Two();
            }}
          >
            <div class="content">
              <c>{this.props.ops.answers[1].a}</c>
            </div>
          </div>

          <div
            id="Three"
            class="item3_5 item"
            onClick={() => {
              this.Three();
            }}
          >
            <div class="content">
              <c>{this.props.ops.answers[2].a}</c>
            </div>
          </div>

          <div
            id="Four"
            class="item4_5 item"
            onClick={() => {
              this.Four();
            }}
          >
            <div class="content">
              <c>{this.props.ops.answers[3].a}</c>
            </div>
          </div>

          <div id="wrapper5">
            <div
              id="Five"
              class="item5_5 item"
              onClick={() => {
                this.Five();
              }}
            >
              <div class="content">
                <c>{this.props.ops.answers[4].a}</c>
              </div>
            </div>
          </div>

          <div id="center" />
        </div>
      </div>
    );
  }
}

export default Wheel_5;
