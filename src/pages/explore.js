import React from 'react';
import { Link } from 'react-router-dom'

class ExplorePage extends React.Component{
    render(){
	return(
	    <div>
		<h1>
		    Explore Page
		</h1>
		<p>
		Here our user will answer a series of questions that originate from large major clusters (i.e. Liberal Arts, Health) and narrow down to specific occupations (i.e. Anesthesiologist).
		</p>

		<p>
		Once they've boiled down a specific occupation, the page will begin to display the general information outlined from Harsh's prototype (i.e. expected salary). Next, we will display the different reccomendations of programs associated witht that occupation (i.e. Hospital Internship). These would ultimately be items that the user can add to their own timeline.
		</p>

		<Link to="/">Home</Link><br />
	    </div>
	)
    }
};

export default ExplorePage