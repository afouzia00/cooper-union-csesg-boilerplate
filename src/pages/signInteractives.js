import React from "react";
import { Link } from 'react-router-dom';
import AnchorLink from 'react-anchor-link-smooth-scroll';

import Wheel_6 from "../components/Wheel.js";

import Wheel_9 from "../components/Wheel9.js";

import Wheel_4 from "../components/Wheel4.js";

import Wheel_11 from "../components/Wheel11.js";

import NavButton from '../components/navbutton.js';

import GoalQuestion from "../components/goal-question.js";

import Tiles from "../components/tiles.js";

import TimelineEvent from "../components/timeline-event.js";

import Allwheels from "../components/allwheels.js";

import Results from "../components/results.js";







// var ReactCSStransition = React.addons.CSStransition;

var opsList1 = {
  q1: {
    question: "Where are you right now?",
    type: "wheel_6",
    answers: [
      {
        a: "Highschool",
        next: "q2"
      },

      {
        a: "2-year college",
        next: "q3"
      },
      {
        a: "4-year college",
        next: "q7"
      },
      {
        a: "Working",
        next: "q9"
      },
      {
        a: "Entrepreneur",
        next: "q9"
      },
      {
        a: "Advanced Degree",
        next: "q15"
      }
    ]
  },

  q2: {
    question: "What grade are you in?",
    type: "wheel_4",
    answers: [
      {
        a: "Freshman",
        next: "END"
      },
      {
        a: "Sophmore",
        next: "END"
      },
      {
        a: "Junior",
        next: "END"
      },
      {
        a: "Senior",
        next: "END"
      }
    ]
  },

  q3: {
    question: "What type of Junior College do you attend?",
    type: "wheel_4",
    answers: [
      {
        a: "Community College",
        next: "q4"
      },
      {
        a: "Technical College",
        next: "q5"
      },
      {
        a: "Career College",
        next: "q6"
      },
      {
        a: "Transfer",
        next: "q4"
      }
    ]
  },

  q4: {
    type: "wheel_6",
    question: "What are you studying at Community College?",
    answers: [
      {
        a: "A",
        next: "END"
      },
      {
        a: "B",
        next: "END"
      },
      {
        a: "C",
        next: "END"
      },
      {
        a: "D",
        next: "END"
      },
      {
        a: "E",
        next: "END"
      },
      {
        a: "F",
        next: "END"
      }
    ]
  },

  q5: {
    type: "wheel_4",
    question: "What are you studying at Vocational College?",
    answers: [
      {
        a: "Construction Trades",
        next: "END"
      },
      {
        a: "Mechanic and Repair Technologies",
        next: "END"
      },
      {
        a: "Precision Production Trades",
        next: "END"
      },
      {
        a: "Transportation and Materials Moving",
        next: "END"
      }
    ]
  },

  q6: {
    type: "wheel_4",
    question: "What are you studying at Career College?",
    answers: [
      {
        a: "Cosmetology",
        next: "END"
      },
      {
        a: "Culinary Arts",
        next: "END"
      },
      {
        a: "Funeral Service and Mortuary Science",
        next: "END"
      },
      {
        a: "General",
        next: "END"
      }
    ]
  },

  q9: {
    type: "tiles",
    question:
      "Which of the following fields do you have a career in?",
    answers: [
      {
        a: "Arts, Entertainment, and Sports",
        next: "q2A"
      },
      {
        a: "Business",
        next: "q5A"
      },
      {
        a: "Health and medicine",
        next: "q11A"
      },
      {
        a: "Media and Social Sciences",
        next: "q15A"
      },
      {
        a: "Public and Social Services",
        next: "q18A"
      },
      {
        a: "Science, Math, and Technology",
        next: "q23A"
      },
      {
        a: "Trades and Personal Services",
        next: "q28A"
      }
    ]
  },

  q2A: {
    type: "tiles",
    question:
      "Which area of Arts, Entertainment, and Sports, do you pursue?",
    answers: [
      {
        a: "Arts, Visual and Performing",
        next: "q3A"
      },
      {
        a: "Sports and Fitnes",
        next: "q4A"
      }
    ]
  },

  q3A: {
    type: "tiles",
    question: "Which area of Arts, Visual, and Performing, do you pursue?",
    answers: [
      {
        a: "Actor",
        next: "q8"
      },
      {
        a: "Art Director",
        next: "q8"
      },
      {
        a: "Choreographer",
        next: "q8"
      },
      {
        a: "Componser",
        next: "q8"
      },
      {
        a: "Craft Artist",
        next: "q8"
      },
      {
        a: "Dancer",
        next: "q8"
      },
      {
        a: "Director",
        next: "q8"
      },
      {
        a: "Fashion Desinger",
        next: "q8"
      },
      {
        a: "Fine Artist",
        next: "q8"
      },
      {
        a: "Floral Designer",
        next: "q8"
      },
      {
        a: "Graphic Designer",
        next: "q8"
      },
      {
        a: "Illustrator",
        next: "q8"
      },
      {
        a: "Industrial Designer",
        next: "q8"
      },
      {
        a: "Interior Designer",
        next: "q8"
      },
      {
        a: "Multimedia Artists and Animator",
        next: "q8"
      },
      {
        a: "Musicians and Singer",
        next: "q8"
      },
      {
        a: "Photographer",
        next: "q8"
      },
      {
        a: "Set Designer",
        next: "q8"
      },
      {
        a: "Theater, Film, and TV Technician",
        next: "q8"
      },
      {
        a: "Web Designer",
        next: "q8"
      }
    ]
  },

  q4A: {
    type: "wheel_4",
    question: "Which area of Sports and Fitness do you pursue?",
    answers: [
      {
        a: "Athletes",
        next: "q8"
      },
      {
        a: "Coaches and Scouts",
        next: "q8"
      },
      {
        a: "Recreation and Fitness Workers",
        next: "q8"
      },
      {
        a: "Umpires and Referees",
        next: "q8"
      }
    ]
  },

  q5A: {
    type: "wheel_5",
    question: "Which area of Business do you pursue?",
    answers: [
      {
        a: "Agriculture",
        next: "q6A"
      },
      {
        a: "Business and Finance",
        next: "q7A"
      },
      {
        a: "Management",
        next: "q8A"
      },
      {
        a: "Office and Administrative Support",
        next: "q9A"
      },
      {
        a: "Sales",
        next: "q10A"
      }
    ]
  },

  q6A: {
    type: "wheel_5",
    question: "Which area of Agriculture do you pursue?",
    answers: [
      {
        a: "Aquaculturists",
        next: "q8"
      },
      {
        a: "Crop Farmers",
        next: "q8"
      },
      {
        a: "Dairy Farmers",
        next: "q8"
      },
      {
        a: "Pig and Poultry Farmers",
        next: "q8"
      },
      {
        a: "Ranchers",
        next: "q8"
      }
    ]
  },

  q7A: {
    type: "tiles",
    question: "Which area of Business and Finance do you pursue?",
    answers: [
      {
        a: "Budget Analysts",
        next: "q8"
      },
      {
        a: "Buyers and Purchasers",
        next: "q8"
      },
      {
        a: "Claims Adjusters, Examiners, and Investigators",
        next: "q8"
      },
      {
        a: "Cost Estimators",
        next: "q8"
      },
      {
        a: "Financial Analysts",
        next: "q8"
      },
      {
        a: "Government Accountants and Auditors",
        next: "q8"
      },
      {
        a: "Insurance Underwriters",
        next: "q8"
      },
      {
        a: "Loan Officers and Counselors",
        next: "q8"
      },
      {
        a: "Management Accountants and Internal Auditors",
        next: "q8"
      },
      {
        a: "Meeting and Convention Planners",
        next: "q8"
      },
      {
        a: "Peronal Financial Advisors",
        next: "q8"
      },
      {
        a: "Public Accountants",
        next: "q8"
      },
      {
        a: "Real estate Appraisers",
        next: "q8"
      },
      {
        a: "Tax Examiners, Collectors, and Revenue Agent",
        next: "q8"
      }
    ]
  },

  q8A: {
    type: "tiles",
    question: "Which area of Management do you pursue?",
    answers: [
      {
        a: "Administrative Services Managers",
        next: "q8"
      },
      {
        a: "Advertising, Marketing, and Public Relations Managers",
        next: "q8"
      },
      {
        a: "Arts Administrators",
        next: "q8"
      },
      {
        a: "Computer and Information Systems Managers",
        next: "q8"
      },
      {
        a: "Construction Managers",
        next: "q8"
      },
      {
        a: "Education Administrators",
        next: "q8"
      },
      {
        a: "Engineering and Science Managers",
        next: "q8"
      },
      {
        a: "Financial Managers",
        next: "q8"
      },
      {
        a: "Food Service Managers",
        next: "q8"
      },
      {
        a: "Funeral Directors",
        next: "q8"
      },
      {
        a: "Human Reources Managers",
        next: "q8"
      },
      {
        a: "Industrial Production Managers",
        next: "q8"
      },
      {
        a: "Labor Relations Managers",
        next: "q8"
      },
      {
        a: "Lodging Managers",
        next: "q8"
      },
      {
        a: "Management Consultants",
        next: "q8"
      },
      {
        a: "Medical and Health Services Managers",
        next: "q8"
      },
      {
        a: "Property Managers",
        next: "q8"
      },
      {
        a: "Top Executives",
        next: "q8"
      }
    ]
  },

  q9A: {
    type: "tiles",
    question:
      "Which area of Office and Administrative Support do you pursue?",
    answers: [
      {
        a: "Administrative Assistants and Secretaries",
        next: "q8"
      },
      {
        a: "Brokerage Clerks",
        next: "q8"
      },
      {
        a: "Arts Administrators",
        next: "q8"
      },
      {
        a: "Computer Operators",
        next: "q8"
      },
      {
        a: "Desktop Publishers",
        next: "q8"
      },
      {
        a: "Eligibility Interviews",
        next: "q8"
      },
      {
        a: "Financial Clerks",
        next: "q8"
      },
      {
        a: "General Office Clerks",
        next: "q8"
      },
      {
        a: "Human Resources Assistants",
        next: "q8"
      },
      {
        a: "Reservation and Transportation Ticket Agents and Travel Clerks",
        next: "q8"
      }
    ]
  },

  q10A: {
    type: "tiles",
    question: "Which area of Sales  do you pursue?",
    answers: [
      {
        a: "Advertising Sales Agents",
        next: "q8"
      },
      {
        a: "Insurance Sales Agents",
        next: "q8"
      },
      {
        a: "Real estate Brokers and Sales Agents",
        next: "q8"
      },
      {
        a: "Sales Engineers",
        next: "q8"
      },
      {
        a: "Sales Representatives, Wholesale and Manufacturing",
        next: "q8"
      },
      {
        a: "Sales Worker Supervisors",
        next: "q8"
      },
      {
        a: "Stockbrokers and Financial Services Sales Agents",
        next: "q8"
      },
      {
        a: "Travel Agents",
        next: "q8"
      }
    ]
  },

  q11A: {
    type: "wheel_3",
    question: "Which area of Health and Medicine  do you pursue?",
    answers: [
      {
        a: "Health Care Support",
        next: "q12A"
      },
      {
        a: "Diagnosis and Treatment",
        next: "q13A"
      },
      {
        a: "Health Technology",
        next: "q14A"
      }
    ]
  },

  q12A: {
    type: "tiles",
    question: "Which area of Health Care Support do you pursue?",
    answers: [
      {
        a: "Dental Assistants",
        next: "q8"
      },
      {
        a: "Massage Therapists",
        next: "q8"
      },
      {
        a: "Medical Assistants",
        next: "q8"
      },
      {
        a: "Medical Transcriptionists",
        next: "q8"
      },
      {
        a: "Nursing, Psychiatric, and Home Health Aides",
        next: "q8"
      },
      {
        a: "Occupational Therapist Assistants",
        next: "q8"
      },
      {
        a: "Physical Therapist Assistants",
        next: "q8"
      },
      {
        a: "Speech-Language Pathology Assistants",
        next: "q8"
      }
    ]
  },

  q13A: {
    type: "tiles",
    question:
      "Which area of Health Diagnosis and Treatment  do you pursue?",
    answers: [
      {
        a: "Advanced-Practice Nurses",
        next: "q8"
      },
      {
        a: "Anesthesiologists",
        next: "q8"
      },
      {
        a: "Chiropractors",
        next: "q8"
      },
      {
        a: "Dentists",
        next: "q8"
      },
      {
        a: "Dietitians and Nutritionists",
        next: "q8"
      },
      {
        a: "General Practitioners",
        next: "q8"
      },
      {
        a: "Gynecologists and Obstetricians",
        next: "q8"
      },
      {
        a: "Internists",
        next: "q8"
      },
      {
        a: "Occupational Therapists",
        next: "q8"
      },
      {
        a: "Optometrists",
        next: "q8"
      },
      {
        a: "Pathologists",
        next: "q8"
      },
      {
        a: "Pediatricians",
        next: "q8"
      },
      {
        a: "Pharmacists",
        next: "q8"
      },
      {
        a: "Physical Therapists",
        next: "q8"
      },
      {
        a: "Physician Assistants",
        next: "q8"
      },
      {
        a: "Podiatrists",
        next: "q8"
      },
      {
        a: "Psychiatrists",
        next: "q8"
      },
      {
        a: "Radiation Therapists",
        next: "q8"
      },
      {
        a: "Radiologists",
        next: "q8"
      },
      {
        a: "Recreational Therapists",
        next: "q8"
      },
      {
        a: "Registered Nurses",
        next: "q8"
      },
      {
        a: "Respitory Therapists",
        next: "q8"
      },
      {
        a: "Speech-Language Pathologists and Audiologists",
        next: "q8"
      },
      {
        a: "Surgeons",
        next: "q8"
      },
      {
        a: "Veterinarians",
        next: "q8"
      }
    ]
  },

  q14A: {
    type: "tiles",
    question: "Which area of Health Technology do you pursue?",
    answers: [
      {
        a: "Athletic Trainers",
        next: "q8"
      },
      {
        a: "Cardiovascular Technologists",
        next: "q8"
      },
      {
        a: "Clinical Laboratory Technologists",
        next: "q8"
      },
      {
        a: "Dental Hygienists",
        next: "q8"
      },
      {
        a: "Diagnostic Medical Sonographers",
        next: "q8"
      },
      {
        a: "Dietetic Technicians",
        next: "q8"
      },
      {
        a: "Emergency Medical Technicians and Paramedics",
        next: "q8"
      },
      {
        a: "Health Information Technicians",
        next: "q8"
      },
      {
        a: "Licensed Practical Nurses",
        next: "q8"
      },
      {
        a: "Medical Billers and Coders",
        next: "q8"
      },
      {
        a: "Nuclear Medicine Technologists",
        next: "q8"
      },
      {
        a: "Occupatoinal Health and Safety Specialists",
        next: "q8"
      },
      {
        a: "Opticians, Dispensing",
        next: "q8"
      },
      {
        a: "Pharmacy Technicians",
        next: "q8"
      },
      {
        a: "Radiologic Technologists",
        next: "q8"
      },
      {
        a: "Surgical Technologists",
        next: "q8"
      },
      {
        a: "Veterinary Technologists and Technicians",
        next: "q8"
      }
    ]
  },

  q15A: {
    type: "tiles",
    question: "Which area of Media and Social Sciences do you pursue?",
    answers: [
      {
        a: "Media and Communications",
        next: "q16A"
      },
      {
        a: "Social Science",
        next: "q17A"
      }
    ]
  },

  q16A: {
    type: "tiles",
    question: "Which area of Media and Communications do you pursue?",
    answers: [
      {
        a: "Announcers",
        next: "q8"
      },
      {
        a: "Broadcast and Sound Engineering Technicians",
        next: "q8"
      },
      {
        a: "Camera Operators and Film and Video Editors",
        next: "q8"
      },
      {
        a: "Copy Editors",
        next: "q8"
      },
      {
        a: "Copywriters",
        next: "q8"
      },
      {
        a: "Editors",
        next: "q8"
      },
      {
        a: "Interpreters",
        next: "q8"
      },
      {
        a: "News Analysts, Reporters, and Correspondents",
        next: "q8"
      },
      {
        a: "Program Directors",
        next: "q8"
      },
      {
        a: "Public Relations Specialists",
        next: "q8"
      },
      {
        a: "Technical Writers",
        next: "q8"
      },
      {
        a: "Translators",
        next: "q8"
      },
      {
        a: "Writers",
        next: "q8"
      }
    ]
  },

  q17A: {
    type: "tiles",
    question: "Which area of Social Science do you pursue?",
    answers: [
      {
        a: "Anthropologists and Archaeologists",
        next: "q8"
      },
      {
        a: "Clinical Psychologists",
        next: "q8"
      },
      {
        a: "Economists",
        next: "q8"
      },
      {
        a: "Geographers",
        next: "q8"
      },
      {
        a: "Historians",
        next: "q8"
      },
      {
        a: "Industrial Psychologists",
        next: "q8"
      },
      {
        a: "Market and Survey Researchers",
        next: "q8"
      },
      {
        a: "Political Scientists",
        next: "q8"
      },
      {
        a: "Research Psychologists",
        next: "q8"
      },
      {
        a: "School Psychologists",
        next: "q8"
      },
      {
        a: "Sociologists",
        next: "q8"
      },
      {
        a: "Urban and Regional Planners",
        next: "q8"
      }
    ]
  },

  q18A: {
    type: "wheel_4",
    question: "Which area of Public and Social Services do you pursue?",
    answers: [
      {
        a: "Community and Social Services",
        next: "q19A"
      },
      {
        a: "Education, Museum Work, and Library Science",
        next: "q20A"
      },
      {
        a: "Law and Government",
        next: "q21A"
      },
      {
        a: "Protective Services",
        next: "q22A"
      }
    ]
  },

  q19A: {
    type: "tiles",
    question: "Which area of Community and Social Services do you pursue?",
    answers: [
      {
        a: "Addiction Counselors",
        next: "q8"
      },
      {
        a: "Child, Family, and School Social Workers",
        next: "q8"
      },
      {
        a: "Educational, Vocational, and School Counselors",
        next: "q8"
      },
      {
        a: "Health Educators",
        next: "q8"
      },
      {
        a: "Human-Service Assistants",
        next: "q8"
      },
      {
        a: "Imams",
        next: "q8"
      },
      {
        a: "Marriage and Family Therapists",
        next: "q8"
      },
      {
        a: "Medical and Public Health Social Workers",
        next: "q8"
      },
      {
        a: "Mental Health and Substance Abuse Social Workers",
        next: "q8"
      },
      {
        a: "Mental Health Counselors",
        next: "q8"
      },
      {
        a: "Nuns and Monks",
        next: "q8"
      },
      {
        a: "Priests",
        next: "q8"
      },
      {
        a: "Probation Officers and Correctional Treatment Specialists",
        next: "q8"
      },
      {
        a: "Protestant Ministers",
        next: "q8"
      },
      {
        a: "Rabbis",
        next: "q8"
      },
      {
        a: "Rehabilitation Counselors",
        next: "q8"
      }
    ]
  },

  q20A: {
    type: "tiles",
    question:
      "Which area of Education, Museum Work, and Library Science do you pursue?",
    answers: [
      {
        a: "Adult Educators",
        next: "q8"
      },
      {
        a: "Archivists",
        next: "q8"
      },
      {
        a: "Conservators",
        next: "q8"
      },
      {
        a: "Curators",
        next: "q8"
      },
      {
        a: "Elementary, Middle, and High School Teachers",
        next: "q8"
      },
      {
        a: "Exhibit Desingers and Museum Technicians",
        next: "q8"
      },
      {
        a: "Instructional Coordinators",
        next: "q8"
      },
      {
        a: "Librarians",
        next: "q8"
      },
      {
        a: "Library Technicians",
        next: "q8"
      },
      {
        a: "Postsecondary Teachers",
        next: "q8"
      },
      {
        a: "Preschool Teachers",
        next: "q8"
      },
      {
        a: "Special Education Teachers",
        next: "q8"
      },
      {
        a: "Teacher Assistants",
        next: "q8"
      },
      {
        a: "Trainers",
        next: "q8"
      }
    ]
  },

  q21A: {
    type: "tiles",
    question: "Which area of Law and Government do you pursue?",
    answers: [
      {
        a: "Community Organizers and Activists",
        next: "q8"
      },
      {
        a: "Court Reporters",
        next: "q8"
      },
      {
        a: "Foreign Service Officers",
        next: "q8"
      },
      {
        a: "Government Executives and Legislators",
        next: "q8"
      },
      {
        a: "Government Lawyers",
        next: "q8"
      },
      {
        a: "In-House Lawyers",
        next: "q8"
      },
      {
        a: "Judges",
        next: "q8"
      },
      {
        a: "Paralegals",
        next: "q8"
      },
      {
        a: "Private-Practice Lawyers",
        next: "q8"
      },
      {
        a: "Public Interest Advocates",
        next: "q8"
      },
      {
        a: "Public Interest Lawyers",
        next: "q8"
      }
    ]
  },

  q22A: {
    type: "tiles",
    question: "Which area of Protective Services do you pursue?",
    answers: [
      {
        a: "Correctional Officers",
        next: "q8"
      },
      {
        a: "Detectives",
        next: "q8"
      },
      {
        a: "Federal Agents",
        next: "q8"
      },
      {
        a: "Firefighters",
        next: "q8"
      },
      {
        a: "Forensic Scientists",
        next: "q8"
      },
      {
        a: "Police Officers",
        next: "q8"
      },
      {
        a: "Private Detectives",
        next: "q8"
      },
      {
        a: "Security Guards",
        next: "q8"
      }
    ]
  },

  q23A: {
    type: "wheel_4",
    question: "Which area of Science, Math, and Technology do you pursue?",
    answers: [
      {
        a: "Architecture, Engineering, and Drafting",
        next: "q24"
      },
      {
        a: "Computers and Math",
        next: "q25"
      },
      {
        a: "Enviornment",
        next: "q26"
      },
      {
        a: "Science",
        next: "q27"
      }
    ]
  },

  q24A: {
    type: "tiles",
    question:
      "Which area of Architecture, Engineering, and Drafting do you pursue?",
    answers: [
      {
        a: "Aerospace Engineer",
        next: "q8"
      },
      {
        a: "Agricultural Engineer",
        next: "q8"
      },
      {
        a: "Architect",
        next: "q8"
      },
      {
        a: "Biomedical Engineer",
        next: "q8"
      },
      {
        a: "Chemical Engineer",
        next: "q8"
      },
      {
        a: "Civil Engineer",
        next: "q8"
      },
      {
        a: "Computer Hardware Engineer",
        next: "q8"
      },
      {
        a: "Drafter",
        next: "q8"
      },
      {
        a: "Electrical Engineer",
        next: "q8"
      },
      {
        a: "Engineering Technician",
        next: "q8"
      },
      {
        a: "Enviornmental Engineer",
        next: "q8"
      },
      {
        a: "Geographic Specialist",
        next: "q8"
      },
      {
        a: "Industrial Engineer",
        next: "q8"
      },
      {
        a: "Landscape Architect",
        next: "q8"
      },
      {
        a: "Materials Engineer",
        next: "q8"
      },
      {
        a: "Mechanical Engineer",
        next: "q8"
      },
      {
        a: "Mining and Geological Engineer",
        next: "q8"
      },
      {
        a: "Nuclear Engineer",
        next: "q8"
      },
      {
        a: "Petroleum Engineer",
        next: "q8"
      }
    ]
  },

  q25A: {
    type: "tiles",
    question: "Which area of Computers and Math do you pursue?",
    answers: [
      {
        a: "Actuaries",
        next: "q8"
      },
      {
        a: "Computer Programmers",
        next: "q8"
      },
      {
        a: "Computer Scientists",
        next: "q8"
      },
      {
        a: "Software Developers",
        next: "q8"
      },
      {
        a: "Computer Support Specialists",
        next: "q8"
      },
      {
        a: "Computer Systems Analysts",
        next: "q8"
      },
      {
        a: "Database Administrators",
        next: "q8"
      },
      {
        a: "Mathematicians",
        next: "q8"
      },
      {
        a: "Network and Computer Systems Administrators",
        next: "q8"
      },
      {
        a: "Network Systems and Data Communicattions Analysts",
        next: "q8"
      },
      {
        a: "Operations Research Analysts",
        next: "q8"
      },
      {
        a: "Statisticians",
        next: "q8"
      }
    ]
  },

  q26A: {
    type: "tiles",
    question: "Which area of Enviornment do you pursue?",
    answers: [
      {
        a: "Conservation Scientists",
        next: "q8"
      },
      {
        a: "Enviornmental Educators",
        next: "q8"
      },
      {
        a: "Enviornmental Inspectors",
        next: "q8"
      },
      {
        a: "Enviornmental Scientists",
        next: "q8"
      },
      {
        a: "Foresters and Forestry Technician",
        next: "q8"
      },
      {
        a: "Park Rangers",
        next: "q8"
      },
      {
        a: "Preserve Managers",
        next: "q8"
      },
      {
        a: "Water- and Wastewater-Treatment-Plant Operators",
        next: "q8"
      },
      {
        a: "Wildlife Technicians",
        next: "q8"
      }
    ]
  },

  q27A: {
    type: "tiles",
    question: "Which area of Science do you pursue?",
    answers: [
      {
        a: "Agricultural and Food Scientists",
        next: "q8"
      },
      {
        a: "Biological Scientists",
        next: "q8"
      },
      {
        a: "Chemists and Materials Scientists",
        next: "q8"
      },
      {
        a: "Geoscientists",
        next: "q8"
      },
      {
        a: "Medical Scientists",
        next: "q8"
      },
      {
        a: "Meterologists",
        next: "q8"
      },
      {
        a: "Physicists and Astronomers",
        next: "q8"
      },
      {
        a: "Science Technicians",
        next: "q8"
      }
    ]
  },

  q28A: {
    type: "tiles",
    question: "Which area of Trades and Personal Services do you pursue?",
    answers: [
      {
        a: "Construction",
        next: "q29A"
      },
      {
        a: "Installation and Repair",
        next: "q30A"
      },
      {
        a: "Personal Care and Culinary Services",
        next: "q31A"
      },
      {
        a: "Production",
        next: "q32A"
      },
      {
        a: "Transportation",
        next: "q33A"
      }
    ]
  },

  q29A: {
    type: "tiles",
    question: "Which area of Construction do you pursue?",
    answers: [
      {
        a: "Boilermarkers",
        next: "q8"
      },
      {
        a: "Bricklayers and Stonemasons",
        next: "q8"
      },
      {
        a: "Carpenters",
        next: "q8"
      },
      {
        a: "Cement Masons and Concrete Finishes",
        next: "q8"
      },
      {
        a: "Construction and Building Inspectors",

        next: "q8"
      },
      {
        a: "Construction Equipment Operators",
        next: "q8"
      },
      {
        a: "Construction Laborers",
        next: "q8"
      },
      {

        a: "Drywall Workers",
        next: "q8"

      },
      {
        a: "Electricians",
        next: "q8"
      },
      {
        a: "elevator Installers and Repairers",
        next: "q8"
      },
      {
        a: "Glaziers",
        next: "q8"
      },
      {
        a: "Hazardous Materials Removal Workers",
        next: "q8"
      },
      {
        a: "Insulation Workers",
        next: "q8"
      },
      {
        a: "Ironworkers",
        next: "q8"
      },
      {
        a: "Painters and Paperhangers",
        next: "q8"
      },
      {
        a: "Plasterers",
        next: "q8"
      },
      {
        a: "Plumbers",
        next: "q8"
      },
      {
        a: "Roofers",
        next: "q8"
      },
      {
        a: "Sheet Metal Workers",
        next: "q8"
      }
    ]
  },

  q30A: {
    type: "tiles",
    question: "Which area of Installation and Repair do you pursue?",
    answers: [
      {
        a: "Aircraft and Avionics Technicians",
        next: "q8"
      },
      {
        a: "ATM Technicians",
        next: "q8"
      },
      {
        a: "Automotive Technicians",
        next: "q8"
      },
      {
        a: "Automotive-Body Repairers",
        next: "q8"
      },
      {
        a: "Computer-Repair Technicians",
        next: "q8"
      },
      {
        a: "Consumer Electronics Technicians",
        next: "q8"
      },
      {
        a: "Diesel Technicians",
        next: "q8"
      },
      {
        a: "Electronics Technicians",
        next: "q8"
      },
      {
        a: "Farm Equipment Mechanics",
        next: "q8"
      },
      {
        a:
          "Heating, Ventilation, Air-Conditioning, and Refigeration Technicians",
        next: "q8"
      },
      {
        a: "Heavy Vehicle and Mobile Equipment Technicians",
        next: "q8"
      },
      {
        a: "Home Appliance Repairers",
        next: "q8"
      },
      {
        a: "Industrial Machinery Repairers",
        next: "q8"
      },
      {
        a: "Line Installers and Repairers",
        next: "q8"
      },
      {
        a: "Office Machine Technicians",
        next: "q8"
      },
      {
        a: "Precision Instrument and Equipment Repairers",
        next: "q8"
      },
      {
        a: "Small Engine Mechanics",
        next: "q8"
      },
      {
        a: "telecommunications and Radio Technicians",
        next: "q8"
      }
    ]
  },

  q31A: {
    type: "tiles",
    question:
      "Which area of Personal Care and Culinary Services do you pursue?",
    answers: [
      {

        a: "Animal Caretakers",
        next: "q8"
      },
      {
        a: "Barbers and Hairstylists",
        next: "q8"
      },
      {
        a: "Chefs",
        next: "q8"

      },
      {
        a: "Child Care Workers",
        next: "q8"
      },
      {
        a: "Flight Attq8ants",
        next: "q8"
      },
      {
        a: "Landscapers and Groundskeepers",
        next: "q8"
      },
      {
        a: "Personal and Home Care Aides",
        next: "q8"
      }
    ]
  },

  q32A: {
    type: "tiles",
    question: "Which area of Production do you pursue?",
    answers: [
      {
        a: "Computer-Control Programmers and Operators",
        next: "q8"
      },
      {
        a: "Dental Laboratory Technicians",
        next: "q8"
      },
      {
        a: "Jewelers and Precious Stone and Metal Workers",
        next: "q8"
      },
      {
        a: "Machinists",
        next: "q8"
      },
      {
        a: "Printing-Machine Operators",
        next: "q8"
      },
      {
        a: "Semiconductor Processors",
        next: "q8"
      },
      {
        a: "Stationary Engineers and Boiler Operators",
        next: "q8"
      },
      {
        a: "Tool and Die Makers",
        next: "q8"
      },
      {
        a: "Welders",
        next: "q8"
      }
    ]
  },

  q33A: {
    type: "wheel_6",
    question: "Which area of Transportation do you pursue?",
    answers: [
      {
        a: "Aircraft Pilots",
        next: "q8"
      },
      {
        a: "Air Traffic Controllers",
        next: "q8"
      },
      {
        a: "Locomotive Engineers",
        next: "q8"
      },
      {
        a: "Railroad Conductors",
        next: "q8"
      },
      {
        a: "Ship Captains and Marine Pilots",
        next: "q8"
      },
      {
        a: "Ship Engineers",
        next: "q8"
      }
    ]
  },

  q8: {
    type: "wheel_5",
    question: "What level of education do you have?",
    answers: [
      {
        a: "Did not graduate high school",
        next: "END"
      },

      {
        a: "Highschool",
        next: "END"

      },
      {
        a: "2-year college",
        next: "q10"
      },
      {
        a: "4-year college",
        next: "q11"
      },
      {
        a: "Advanced degree",
        next: "q12"
      }
    ]
  },

  q10: {
    type: "wheel_4",
    question: "What type of Junior College do you attend?",
    answers: [
      {
        a: "Community College",
        next: "q13"
      },
      {
        a: "Vocational-Technical College",
        next: "q14"
      },
      {
        a: "Career College",
        next: "q15"
      },
      {
        a: "Community College Transfer",
        next: "q13"
      }
    ]
  },

  q13: {
    type: "wheel_6",
    question: "What are you studying at Community College?",
    answers: [
      {
        a: "A",
        next: "END"
      },
      {
        a: "B",
        next: "END"
      },
      {
        a: "C",
        next: "END"
      },
      {
        a: "D",
        next: "END"
      },
      {
        a: "E",
        next: "END"
      },
      {
        a: "F",
        next: "END"
      }
    ]
  },

  q14: {
    type: "wheel_6",
    question: "What are you studying at Vocational College?",
    answers: [
      {
        a: "A",
        next: "END"
      },
      {
        a: "B",
        next: "END"
      },
      {
        a: "C",
        next: "END"
      },
      {
        a: "D",
        next: "END"
      },
      {
        a: "E",
        next: "END"
      },
      {
        a: "F",
        next: "END"
      }
    ]
  },

  q7: {
    type: "wheel_5",
    question: "What are you majoring in at college?",
    answers: [
      {
        a: "Medical and Life Sciences",
        next: "q16"
      },
      {
        a: "Visual and Performance Arts",
        next: "q17"
      },
      {
        a: "Liberal Arts",
        next: "q18"
      },
      {
        a: "Engineering and Technology",
        next: "q19"
      },
      {
        a: "Business",
        next: "q20"
      },

      {

        a: "Political Science",
        next: "END"
      }

    ]
  },

  q16: {
    type: "tiles",
    question: "What specific Medical and Life Science major?",
    answers: [
      {
        a: "Athletic Training",
        next: "END"
      },
      {
        a: "Biology",
        next: "END"
      },
      {
        a: "Chemistry",
        next: "END"
      },
      {
        a: "Enviornmental Science",
        next: "END"
      },
      {
        a: "Exercise Science or Kinesiology",
        next: "END"
      },
      {
        a: "Fisheries and Wildlife",
        next: "END"
      },
      {
        a: "Food Science",
        next: "END"
      },
      {
        a: "Forest Management",
        next: "END"
      },
      {
        a: "Marine Science",
        next: "END"
      },
      {
        a: "Nursing",
        next: "END"
      },
      {
        a: "Organic/Urban Farming",
        next: "END"
      },
      {
        a: "Pharmacy",
        next: "END"
      },
      {
        a: "Studying to be a Physicians Assistant",
        next: "END"
      },
      {
        a: "Pre-Dental",
        next: "END"
      },
      {
        a: "Pre-Medical",
        next: "END"
      },
      {
        a: "Pre-Veterinary Medicine",
        next: "END"
      }
    ]
  },

  q17: {
    type: "tiles",
    question: "What Visual and Performance Arts major?",
    answers: [
      {
        a: "Apparel/Textile Design",
        next: "END"
      },
      {
        a: "Architecture",
        next: "END"
      },
      {
        a: "Dance",
        next: "END"
      },
      {
        a: "Film/Broadcast",
        next: "END"
      },
      {
        a: "Fine/Studio Art",
        next: "END"
      },
      {
        a: "Graphic Design",
        next: "END"
      },
      {
        a: "Industrial Design",
        next: "END"
      },
      {
        a: "Interior Design",
        next: "END"
      },
      {
        a: "Landscape Architecture",
        next: "END"
      },
      {
        a: "Music",
        next: "END"
      },
      {
        a: "Theatre",
        next: "END"
      },
      {
        a: "Urban Planning",
        next: "END"
      },
      {
        a: "Video Game Design",
        next: "END"
      },
      {
        a: "Web Design/Digital Media",
        next: "END"
      }
    ]
  },

  q18: {
    type: "tiles",
    question: "What Liberal Arts major?",
    answers: [
      {
        a: "Arts Management",
        next: "END"
      },
      {
        a: "Education",
        next: "END"
      },
      {
        a: "Emergency Management",
        next: "END"
      },
      {
        a: "English/Writing",
        next: "END"
      },
      {
        a: "Equine Science/Management",
        next: "END"
      },
      {
        a: "Family and Child Science",
        next: "END"
      },
      {
        a: "History",
        next: "END"
      },
      {
        a: "Journalism",
        next: "END"
      },
      {
        a: "Language Studies",
        next: "END"
      },
      {
        a: "Non-Profit Management",
        next: "END"
      },
      {
        a: "Peace/Conflict Studies",
        next: "END"
      },
      {
        a: "Philosophy",
        next: "END"
      },
      {
        a: "Political Science",
        next: "END"
      },
      {
        a: "Social Science",
        next: "END"
      },
      {
        a: "Sports Turf/Golf Management",
        next: "END"
      },
      {
        a: "Women/Gender Studies",
        next: "END"
      }
    ]
  },

  q19: {
    type: "tiles",
    question: "What Engineering and Technology major?",
    answers: [
      {
        a: "Aerospace Engineering",
        next: "END"
      },
      {
        a: "Astronomy",
        next: "END"
      },
      {
        a: "Aviation/Aeronautics",
        next: "END"
      },
      {
        a: "Biomedical Engineering",
        next: "END"
      },
      {
        a: "Chemical Engineering",
        next: "END"
      },
      {
        a: "Civil Engineering",
        next: "END"
      },
      {
        a: "Computer Science",
        next: "END"
      },
      {
        a: "Electrical Engineering",
        next: "END"
      },
      {
        a: "Energy Science",
        next: "END"
      },
      {
        a: "Engineering",
        next: "END"
      },
      {
        a: "Imaging Science",
        next: "END"
      },
      {
        a: "Industrial Engineering",
        next: "END"
      },
      {
        a: "Industrial Technology",
        next: "END"
      },
      {
        a: "Materials Science",
        next: "END"
      },
      {
        a: "Mathematics",
        next: "END"
      },
      {
        a: "Mechanical Engineering",
        next: "END"
      }
    ]
  },

  q20: {
    type: "tiles",
    question: "What Business major?",
    answers: [
      {
        a: "Accounting-General",
        next: "END"
      },
      {
        a: "Business-General",
        next: "END"
      },
      {
        a: "Construction Management",
        next: "END"
      },
      {
        a: "Finance and Economics",
        next: "END"
      },
      {
        a: "Hospitality Management",
        next: "END"
      },
      {
        a: "Human Resources Management",
        next: "END"
      },
      {
        a: "Information Systems",
        next: "END"
      },
      {
        a: "Insurance and Risk Management",
        next: "END"
      },
      {
        a: "National Parks Management",
        next: "END"
      },
      {
        a: "Public Health Administration",
        next: "END"
      },
      {
        a: "Sport Management",
        next: "END"
      },
      {
        a: "Logistics",
        next: "END"
      }
    ]
  },

  q12: {
    type: "wheel_4",
    question: "What kind of advanced degree do you have?",
    answers: [
      {
        a: "Graduate Certificate",
        next: "END"
      },
      {
        a: "Graduate Diploma",
        next: "END"
      },
      {
        a: "Masters",
        next: "END"
      },
      {
        a: "Professional Doctorates",
        next: "END"
      }
    ]
  },

  q15: {
    type: "wheel_4",
    question: "What kind of advanced degree do you have?",
    answers: [
      {
        a: "Graduate Certificate",
        next: "END"
      },
      {
        a: "Graduate Diploma",
        next: "END"
      },
      {
        a: "Masters",
        next: "END"
      },
      {
        a: "Professional Doctorates",
        next: "END"
      }
    ]
  }
};

var opsList1_answersArray = [];

var opsList2_questionsAnswered = [];

var opsList2 = {
  q1: {
    type: "tiles",
    question:
      "Which of the following fields would you like to have a career in?",
    answers: [
      {
        a: "Arts, Entertainment, and Sports",
        next: "q2"
      },
      {
        a: "Business",
        next: "q5"
      },
      {
        a: "Health and medicine",
        next: "q11"
      },
      {
        a: "Media and Social Sciences",
        next: "q15"
      },
      {
        a: "Public and Social Services",
        next: "q18"
      },
      {
        a: "Science, Math, and Technology",
        next: "q23"
      },
      {
        a: "Trades and Personal Services",
        next: "q28"
      }
    ]
  },

  q2: {
    type: "tiles",
    question:
      "Which area of Arts, Entertainment, and Sports, most interests you?",
    answers: [
      {
        a: "Arts, Visual and Performing",
        next: "q3"
      },
      {
        a: "Sports and Fitnes",
        next: "q4"
      }
    ]
  },

  q3: {
    type: "tiles",
    question: "Which area of Arts, Visual, and Performing, most interests you?",
    answers: [
      {
        a: "Actor",
        next: "END"
      },
      {
        a: "Art Director",
        next: "END"
      },
      {
        a: "Choreographer",
        next: "END"
      },
      {
        a: "Componser",
        next: "END"
      },
      {
        a: "Craft Artist",
        next: "END"
      },
      {
        a: "Dancer",
        next: "END"
      },
      {
        a: "Director",
        next: "END"
      },
      {
        a: "Fashion Desinger",
        next: "END"
      },
      {
        a: "Fine Artist",
        next: "END"
      },
      {
        a: "Floral Designer",
        next: "END"
      },
      {
        a: "Graphic Designer",
        next: "END"
      },
      {
        a: "Illustrator",
        next: "END"
      },
      {
        a: "Industrial Designer",
        next: "END"
      },
      {
        a: "Interior Designer",
        next: "END"
      },
      {
        a: "Multimedia Artist and Animator",
        next: "END"
      },
      {
        a: "Musicians and Singer",
        next: "END"
      },
      {
        a: "Photographer",
        next: "END"
      },
      {
        a: "Set Designer",
        next: "END"
      },
      {
        a: "Theater, Film, and TV Technician",
        next: "END"
      },
      {
        a: "Web Designer",
        next: "END"
      }
    ]
  },

  q4: {
    type: "wheel_4",
    question: "Which area of Sports and Fitness most interests you?",
    answers: [
      {
        a: "Athletes",
        next: "END"
      },
      {
        a: "Coaches and Scouts",
        next: "END"
      },
      {
        a: "Recreation and Fitness Workers",
        next: "END"
      },
      {
        a: "Umpires and Referees",
        next: "END"
      }
    ]
  },

  q5: {
    type: "wheel_5",
    question: "Which area of Business most interests you?",
    answers: [
      {
        a: "Agriculture",
        next: "q6"
      },
      {
        a: "Business and Finance",
        next: "q7"
      },
      {
        a: "Management",
        next: "q8"
      },
      {
        a: "Administrative Support",
        next: "q9"
      },
      {
        a: "Sales",
        next: "q10"
      }
    ]
  },

  q6: {
    type: "tiles",
    question: "Which area of Agriculture most interests you?",
    answers: [
      {
        a: "Aquaculturists",
        next: "END"
      },
      {
        a: "Crop Farmers",
        next: "END"
      },
      {
        a: "Dairy Farmers",
        next: "END"
      },
      {
        a: "Pig and Poultry Farmers",
        next: "END"
      },
      {
        a: "Ranchers",
        next: "END"
      }
    ]
  },

  q7: {
    type: "tiles",
    question: "Which area of Business and Finance most interests you?",
    answers: [
      {
        a: "Budget Analysts",
        next: "END"
      },
      {
        a: "Buyers and Purchasers",
        next: "END"
      },
      {
        a: "Claims Adjusters, Examiners, and Investigators",
        next: "END"
      },
      {
        a: "Cost Estimators",
        next: "END"
      },
      {
        a: "Financial Analysts",
        next: "END"
      },
      {
        a: "Government Accountants and Auditors",
        next: "END"
      },
      {
        a: "Insurance Underwriters",
        next: "END"
      },
      {
        a: "Loan Officers and Counselors",
        next: "END"
      },
      {
        a: "Management Accountants and Internal Auditors",
        next: "END"
      },
      {
        a: "Meeting and Convention Planners",
        next: "END"
      },
      {
        a: "Peronal Financial Advisors",
        next: "END"
      },
      {
        a: "Public Accountants",
        next: "END"
      },
      {
        a: "Real estate Appraisers",
        next: "END"
      },
      {
        a: "Tax Examiners, Collectors, and Revenue Agent",
        next: "END"
      }
    ]
  },

  q8: {
    type: "tiles",
    question: "Which area of Management most interests you?",
    answers: [
      {
        a: "Administrative Services Managers",
        next: "END"
      },
      {
        a: "Advertising, Marketing, and Public Relations Managers",
        next: "END"
      },
      {
        a: "Arts Administrators",
        next: "END"
      },
      {
        a: "Computer and Information Systems Managers",
        next: "END"
      },
      {
        a: "Construction Managers",
        next: "END"
      },
      {
        a: "Education Administrators",
        next: "END"
      },
      {
        a: "Engineering and Science Managers",
        next: "END"
      },
      {
        a: "Financial Managers",
        next: "END"
      },
      {
        a: "Food Service Managers",
        next: "END"
      },
      {
        a: "Funeral Directors",
        next: "END"
      },
      {
        a: "Human Reources Managers",
        next: "END"
      },
      {
        a: "Industrial Production Managers",
        next: "END"
      },
      {
        a: "Labor Relations Managers",
        next: "END"
      },
      {
        a: "Lodging Managers",
        next: "END"
      },
      {
        a: "Management Consultants",
        next: "END"
      },
      {
        a: "Medical and Health Services Managers",
        next: "END"
      },
      {
        a: "Property Managers",
        next: "END"
      },
      {
        a: "Top Executives",
        next: "END"
      }
    ]
  },

  q9: {
    type: "tiles",
    question:
      "Which area of Office and Administrative Support most interests you?",
    answers: [
      {
        a: "Administrative Assistants and Secretaries",
        next: "END"
      },
      {
        a: "Brokerage Clerks",
        next: "END"
      },
      {
        a: "Arts Administrators",
        next: "END"
      },
      {
        a: "Computer Operators",
        next: "END"
      },
      {
        a: "Desktop Publishers",
        next: "END"
      },
      {
        a: "Eligibility Interviews",
        next: "END"
      },
      {
        a: "Financial Clerks",
        next: "END"
      },
      {
        a: "General Office Clerks",
        next: "END"
      },
      {
        a: "Human Resources Assistants",
        next: "END"
      },
      {
        a: "Reservation and Transportation Ticket Agents and Travel Clerks",
        next: "END"
      }
    ]
  },

  q10: {
    type: "tiles",
    question: "Which area of Sales most interests you?",
    answers: [
      {
        a: "Advertising Sales Agents",
        next: "END"
      },
      {
        a: "Insurance Sales Agents",
        next: "END"
      },
      {
        a: "Real estate Brokers and Sales Agents",
        next: "END"
      },
      {
        a: "Sales Engineers",
        next: "END"
      },
      {
        a: "Sales Representatives, Wholesale and Manufacturing",
        next: "END"
      },
      {
        a: "Sales Worker Supervisors",
        next: "END"
      },
      {
        a: "Stockbrokers and Financial Services Sales Agents",
        next: "END"
      },
      {
        a: "Travel Agents",
        next: "END"
      }
    ]
  },

  q11: {
    type: "wheel_3",
    question: "Which area of Health and Medicine most interests you?",
    answers: [
      {
        a: "Health Care Support",
        next: "q12"
      },
      {
        a: "Diagnosis and Treatment",
        next: "q13"
      },
      {
        a: "Health Technology",
        next: "q14"
      }
    ]
  },

  q12: {
    type: "tiles",
    question: "Which area of Health Care Support most interests you?",
    answers: [
      {
        a: "Dental Assistants",
        next: "END"
      },
      {
        a: "Massage Therapists",
        next: "END"
      },
      {
        a: "Medical Assistants",
        next: "END"
      },
      {
        a: "Medical Transcriptionists",
        next: "END"
      },
      {
        a: "Nursing, Psychiatric, and Home Health Aides",
        next: "END"
      },
      {
        a: "Occupational Therapist Assistants",
        next: "END"
      },
      {
        a: "Physical Therapist Assistants",
        next: "END"
      },
      {
        a: "Speech-Language Pathology Assistants",
        next: "END"
      }
    ]
  },

  q13: {
    type: "tiles",
    question:
      "Which area of Health Diagnosis and Treatment most interests you?",
    answers: [
      {
        a: "Advanced-Practice Nurses",
        next: "END"
      },
      {
        a: "Anesthesiologists",
        next: "END"
      },
      {
        a: "Chiropractors",
        next: "END"
      },
      {
        a: "Dentists",
        next: "END"
      },
      {
        a: "Dietitians and Nutritionists",
        next: "END"
      },
      {
        a: "General Practitioners",
        next: "END"
      },
      {
        a: "Gynecologists and Obstetricians",
        next: "END"
      },
      {
        a: "Internists",
        next: "END"
      },
      {
        a: "Occupational Therapists",
        next: "END"
      },
      {
        a: "Optometrists",
        next: "END"
      },
      {
        a: "Pathologists",
        next: "END"
      },
      {
        a: "Pediatricians",
        next: "END"
      },
      {
        a: "Pharmacists",
        next: "END"
      },
      {
        a: "Physical Therapists",
        next: "END"
      },
      {
        a: "Physician Assistants",
        next: "END"
      },
      {
        a: "Podiatrists",
        next: "END"
      },
      {
        a: "Psychiatrists",
        next: "END"
      },
      {
        a: "Radiation Therapists",
        next: "END"
      },
      {
        a: "Radiologists",
        next: "END"
      },
      {
        a: "Recreational Therapists",
        next: "END"
      },
      {
        a: "Registered Nurses",
        next: "END"
      },
      {
        a: "Respitory Therapists",
        next: "END"
      },
      {
        a: "Speech-Language Pathologists and Audiologists",
        next: "END"
      },
      {
        a: "Surgeons",
        next: "END"
      },
      {
        a: "Veterinarians",
        next: "END"
      }
    ]
  },

  q14: {
    type: "tiles",
    question: "Which area of Health Technology most interests you?",
    answers: [
      {
        a: "Athletic Trainers",
        next: "END"
      },
      {
        a: "Cardiovascular Technologists",
        next: "END"
      },
      {
        a: "Clinical Laboratory Technologists",
        next: "END"
      },
      {
        a: "Dental Hygienists",
        next: "END"
      },
      {
        a: "Diagnostic Medical Sonographers",
        next: "END"
      },
      {
        a: "Dietetic Technicians",
        next: "END"
      },
      {
        a: "Emergency Medical Technicians and Paramedics",
        next: "END"
      },
      {
        a: "Health Information Technicians",
        next: "END"
      },
      {
        a: "Licensed Practical Nurses",
        next: "END"
      },
      {
        a: "Medical Billers and Coders",
        next: "END"
      },
      {
        a: "Nuclear Medicine Technologists",
        next: "END"
      },
      {
        a: "Occupatoinal Health and Safety Specialists",
        next: "END"
      },
      {
        a: "Opticians, Dispensing",
        next: "END"
      },
      {
        a: "Pharmacy Technicians",
        next: "END"
      },
      {
        a: "Radiologic Technologists",
        next: "END"
      },
      {
        a: "Surgical Technologists",
        next: "END"
      },
      {
        a: "Veterinary Technologists and Technicians",
        next: "END"
      }
    ]
  },

  q15: {
    type: "tiles",
    question: "Which area of Media and Social Sciences most interests you?",
    answers: [
      {
        a: "Media and Communications",
        next: "q16"
      },
      {
        a: "Social Science",
        next: "q17"
      }
    ]
  },

  q16: {
    type: "tiles",
    question: "Which area of Media and Communications most interests you?",
    answers: [
      {
        a: "Announcers",
        next: "END"
      },
      {
        a: "Broadcast and Sound Engineering Technicians",
        next: "END"
      },
      {
        a: "Camera Operators and Film and Video Editors",
        next: "END"
      },
      {
        a: "Copy Editors",
        next: "END"
      },
      {
        a: "Copywriters",
        next: "END"
      },
      {
        a: "Editors",
        next: "END"
      },
      {
        a: "Interpreters",
        next: "END"
      },
      {
        a: "News Analysts, Reporters, and Correspondents",
        next: "END"
      },
      {
        a: "Program Directors",
        next: "END"
      },
      {
        a: "Public Relations Specialists",
        next: "END"
      },
      {
        a: "Technical Writers",
        next: "END"
      },
      {
        a: "Translators",
        next: "END"
      },
      {
        a: "Writers",
        next: "END"
      }
    ]
  },

  q17: {
    type: "tiles",
    question: "Which area of Social Science most interests you?",
    answers: [
      {
        a: "Anthropologists and Archaeologists",
        next: "END"
      },
      {
        a: "Clinical Psychologists",
        next: "END"
      },
      {
        a: "Economists",
        next: "END"
      },
      {
        a: "Geographers",
        next: "END"
      },
      {
        a: "Historians",
        next: "END"
      },
      {
        a: "Industrial Psychologists",
        next: "END"
      },
      {
        a: "Market and Survey Researchers",
        next: "END"
      },
      {
        a: "Political Scientists",
        next: "END"
      },
      {
        a: "Research Psychologists",
        next: "END"
      },
      {
        a: "School Psychologists",
        next: "END"
      },
      {
        a: "Sociologists",
        next: "END"
      },
      {
        a: "Urban and Regional Planners",
        next: "END"
      }
    ]
  },

  q18: {
    type: "wheel_4",
    question: "Which area of Public and Social Services most interests you?",
    answers: [
      {
        a: "Community and Social Services",
        next: "q19"
      },
      {
        a: "Education, Museum Work, and Library Science",
        next: "q20"
      },
      {
        a: "Law and Government",
        next: "q21"
      },
      {
        a: "Protective Services",
        next: "q22"
      }
    ]
  },

  q19: {
    type: "tiles",
    question: "Which area of Community and Social Services most interests you?",
    answers: [
      {
        a: "Addiction Counselors",
        next: "END"
      },
      {
        a: "Child, Family, and School Social Workers",
        next: "END"
      },
      {
        a: "Educational, Vocational, and School Counselors",
        next: "END"
      },
      {
        a: "Health Educators",
        next: "END"
      },
      {
        a: "Human-Service Assistants",
        next: "END"
      },
      {
        a: "Imams",
        next: "END"
      },
      {
        a: "Marriage and Family Therapists",
        next: "END"
      },
      {
        a: "Medical and Public Health Social Workers",
        next: "END"
      },
      {
        a: "Mental Health and Substance Abuse Social Workers",
        next: "END"
      },
      {
        a: "Mental Health Counselors",
        next: "END"
      },
      {
        a: "Nuns and Monks",
        next: "END"
      },
      {
        a: "Priests",
        next: "END"
      },
      {
        a: "Probation Officers and Correctional Treatment Specialists",
        next: "END"
      },
      {
        a: "Protestant Ministers",
        next: "END"
      },
      {
        a: "Rabbis",
        next: "END"
      },
      {
        a: "Rehabilitation Counselors",
        next: "END"
      }
    ]
  },

  q20: {
    type: "tiles",
    question:
      "Which area of Education, Museum Work, and Library Science most interests you?",
    answers: [
      {
        a: "Adult Educators",
        next: "END"
      },
      {
        a: "Archivists",
        next: "END"
      },
      {
        a: "Conservators",
        next: "END"
      },
      {
        a: "Curators",
        next: "END"
      },
      {
        a: "Elementary, Middle, and High School Teachers",
        next: "END"
      },
      {
        a: "Exhibit Desingers and Museum Technicians",
        next: "END"
      },
      {
        a: "Instructional Coordinators",
        next: "END"
      },
      {
        a: "Librarians",
        next: "END"
      },
      {
        a: "Library Technicians",
        next: "END"
      },
      {
        a: "Postsecondary Teachers",
        next: "END"
      },
      {
        a: "Preschool Teachers",
        next: "END"
      },
      {
        a: "Special Education Teachers",
        next: "END"
      },
      {
        a: "Teacher Assistants",
        next: "END"
      },
      {
        a: "Trainers",
        next: "END"
      }
    ]
  },

  q21: {
    type: "tiles",
    question: "Which area of Law and Government most interests you?",
    answers: [
      {
        a: "Community Organizers and Activists",
        next: "END"
      },
      {
        a: "Court Reporters",
        next: "END"
      },
      {
        a: "Foreign Service Officers",
        next: "END"
      },
      {
        a: "Government Executives and Legislators",
        next: "END"
      },
      {
        a: "Government Lawyers",
        next: "END"
      },
      {
        a: "In-House Lawyers",
        next: "END"
      },
      {
        a: "Judges",
        next: "END"
      },
      {
        a: "Paralegals",
        next: "END"
      },
      {
        a: "Private-Practice Lawyers",
        next: "END"
      },
      {
        a: "Public Interest Advocates",
        next: "END"
      },
      {
        a: "Public Interest Lawyers",
        next: "END"
      }
    ]
  },

  q22: {
    type: "tiles",
    question: "Which area of Protective Services most interests you?",
    answers: [
      {
        a: "Correctional Officers",
        next: "END"
      },
      {
        a: "Detectives",
        next: "END"
      },
      {
        a: "Federal Agents",
        next: "END"
      },
      {
        a: "Firefighters",
        next: "END"
      },
      {
        a: "Forensic Scientists",
        next: "END"
      },
      {
        a: "Police Officers",
        next: "END"
      },
      {
        a: "Private Detectives",
        next: "END"
      },
      {
        a: "Security Guards",
        next: "END"
      }
    ]
  },

  q23: {
    type: "tiles",
    question: "Which area of Science, Math, and Technology most interests you?",
    answers: [
      {
        a: "Engineering",
        next: "q24"
      },
      {
        a: "Computers and Math",
        next: "q25"
      },
      {
        a: "Enviornment",
        next: "q26"
      },
      {
        a: "Science",
        next: "q27"
      }
    ]
  },

  q24: {
    type: "tiles",
    question:
      "Which area of Architecture, Engineering, and Drafting most interests you?",
    answers: [
      {
        a: "Aerospace Engineer",
        next: "END"
      },
      {
        a: "Agricultural Engineer",
        next: "END"
      },
      {
        a: "Architect",
        next: "END"
      },
      {
        a: "Biomedical Engineer",
        next: "END"
      },
      {
        a: "Chemical Engineer",
        next: "END"
      },
      {
        a: "Civil Engineer",
        next: "END"
      },
      {
        a: "Computer Hardware Engineer",
        next: "END"
      },
      {
        a: "Drafter",
        next: "END"
      },
      {
        a: "Electrical Engineer",
        next: "END"
      },
      {
        a: "Engineering Technician",
        next: "END"
      },
      {
        a: "Enviornmental Engineer",
        next: "END"
      },
      {
        a: "Geographic Specialist",
        next: "END"
      },
      {
        a: "Industrial Engineer",
        next: "END"
      },
      {
        a: "Landscape Architect",
        next: "END"
      },
      {
        a: "Materials Engineer",
        next: "END"
      },
      {
        a: "Mechanical Engineer",
        next: "END"
      },
      {
        a: "Mining and Geological Engineer",
        next: "END"
      },
      {
        a: "Nuclear Engineer",
        next: "END"
      },
      {
        a: "Petroleum Engineer",
        next: "END"
      }
    ]
  },

  q25: {
    type: "tiles",
    question: "Which area of Computers and Math most interests you?",
    answers: [
      {
        a: "Actuaries",
        next: "END"
      },
      {
        a: "Computer Programmers",
        next: "END"
      },
      {
        a: "Computer Scientists",
        next: "END"
      },
      {
        a: "Software Developers",
        next: "END"
      },
      {
        a: "Computer Support Specialists",
        next: "END"
      },
      {
        a: "Computer Systems Analysts",
        next: "END"
      },
      {
        a: "Database Administrators",
        next: "END"
      },
      {
        a: "Mathematicians",
        next: "END"
      },
      {
        a: "Network and Computer Systems Administrators",
        next: "END"
      },
      {
        a: "Network Systems and Data Communicattions Analysts",
        next: "END"
      },
      {
        a: "Operations Research Analysts",
        next: "END"
      },
      {
        a: "Statisticians",
        next: "END"
      }
    ]
  },

  q26: {
    type: "tiles",
    question: "Which area of Enviornment most interests you?",
    answers: [
      {
        a: "Conservation Scientists",
        next: "END"
      },
      {
        a: "Enviornmental Educators",
        next: "END"
      },
      {
        a: "Enviornmental Inspectors",
        next: "END"
      },
      {
        a: "Enviornmental Scientists",
        next: "END"
      },
      {
        a: "Foresters and Forestry Technician",
        next: "END"
      },
      {
        a: "Park Rangers",
        next: "END"
      },
      {
        a: "Preserve Managers",
        next: "END"
      },
      {
        a: "Water- and Wastewater-Treatment-Plant Operators",
        next: "END"
      },
      {
        a: "Wildlife Technicians",
        next: "END"
      }
    ]
  },

  q27: {
    type: "tiles",
    question: "Which area of Science most interests you?",
    answers: [
      {
        a: "Agricultural and Food Scientists",
        next: "END"
      },
      {
        a: "Biological Scientists",
        next: "END"
      },
      {
        a: "Chemists and Materials Scientists",
        next: "END"
      },
      {
        a: "Geoscientists",
        next: "END"
      },
      {
        a: "Medical Scientists",
        next: "END"
      },
      {
        a: "Meterologists",
        next: "END"
      },
      {
        a: "Physicists and Astronomers",
        next: "END"
      },
      {
        a: "Science Technicians",
        next: "END"
      }
    ]
  },

  q28: {
    type: "tiles",
    question: "Which area of Trades and Personal Services most interests you?",
    answers: [
      {
        a: "Construction",
        next: "q29"
      },
      {
        a: "Installation and Repair",
        next: "q30"
      },
      {
        a: "Personal Care and Culinary Services",
        next: "q31"
      },
      {
        a: "Production",
        next: "q32"
      },
      {
        a: "Transportation",
        next: "q33"
      }
    ]
  },

  q29: {
    type: "tiles",
    question: "Which area of Construction most interests you?",
    answers: [
      {
        a: "Boilermarkers",
        next: "END"
      },
      {
        a: "Bricklayers and Stonemasons",
        next: "END"
      },
      {
        a: "Carpenters",
        next: "END"
      },
      {
        a: "Cement Masons and Concrete Finishes",
        next: "END"
      },
      {
        a: "Construction and Building Inspectors",
        next: "END"
      },
      {
        a: "Construction Equipment Operators",
        next: "END"
      },
      {
        a: "Construction Laborers",
        next: "END"
      },
      {
        a: "Drywall Workers",
        next: "END"
      },
      {
        a: "Electricians",
        next: "END"
      },
      {
        a: "elevator Installers and Repairers",
        next: "END"
      },
      {
        a: "Glaziers",
        next: "END"
      },
      {
        a: "Hazardous Materials Removal Workers",
        next: "END"
      },
      {
        a: "Insulation Workers",
        next: "END"
      },
      {
        a: "Ironworkers",
        next: "END"
      },
      {
        a: "Painters and Paperhangers",
        next: "END"
      },
      {
        a: "Plasterers",
        next: "END"
      },
      {
        a: "Plumbers",
        next: "END"
      },
      {
        a: "Roofers",
        next: "END"
      },
      {
        a: "Sheet Metal Workers",
        next: "END"
      }
    ]
  },

  q30: {
    type: "tiles",
    question: "Which area of Installation and Repair most interests you?",
    answers: [
      {
        a: "Aircraft and Avionics Technicians",
        next: "END"
      },
      {
        a: "ATM Technicians",
        next: "END"
      },
      {
        a: "Automotive Technicians",
        next: "END"
      },
      {
        a: "Automotive-Body Repairers",
        next: "END"
      },
      {
        a: "Computer-Repair Technicians",
        next: "END"
      },
      {
        a: "Consumer Electronics Technicians",
        next: "END"
      },
      {
        a: "Diesel Technicians",
        next: "END"
      },
      {
        a: "Electronics Technicians",
        next: "END"
      },
      {
        a: "Farm Equipment Mechanics",
        next: "END"
      },
      {
        a:
          "Heating, Ventilation, Air-Conditioning, and Refigeration Technicians",
        next: "END"
      },
      {
        a: "Heavy Vehicle and Mobile Equipment Technicians",
        next: "END"
      },
      {
        a: "Home Appliance Repairers",
        next: "END"
      },
      {
        a: "Industrial Machinery Repairers",
        next: "END"
      },
      {
        a: "Line Installers and Repairers",
        next: "END"
      },
      {
        a: "Office Machine Technicians",
        next: "END"
      },
      {
        a: "Precision Instrument and Equipment Repairers",
        next: "END"
      },
      {
        a: "Small Engine Mechanics",
        next: "END"
      },
      {
        a: "telecommunications and Radio Technicians",
        next: "END"
      }
    ]
  },

  q31: {
    type: "tiles",
    question:
      "Which area of Personal Care and Culinary Services most interests you?",
    answers: [
      {
        a: "Animal Caretakers",
        next: "END"
      },
      {
        a: "Barbers and Hairstylists",
        next: "END"
      },
      {
        a: "Chefs",
        next: "END"
      },
      {
        a: "Child Care Workers",
        next: "END"
      },
      {
        a: "Flight Attendants",
        next: "END"
      },
      {
        a: "Landscapers and Groundskeepers",
        next: "END"
      },
      {
        a: "Personal and Home Care Aides",
        next: "END"
      }
    ]
  },

  q32: {
    type: "tiles",
    question: "Which area of Production most interests you?",
    answers: [
      {
        a: "Computer-Control Programmers and Operators",
        next: "END"
      },
      {
        a: "Dental Laboratory Technicians",
        next: "END"
      },
      {
        a: "Jewelers and Precious Stone and Metal Workers",
        next: "END"
      },
      {
        a: "Machinists",
        next: "END"
      },
      {
        a: "Printing-Machine Operators",
        next: "END"
      },
      {
        a: "Semiconductor Processors",
        next: "END"
      },
      {
        a: "Stationary Engineers and Boiler Operators",
        next: "END"
      },
      {
        a: "Tool and Die Makers",
        next: "END"
      },
      {
        a: "Welders",
        next: "END"
      }
    ]
  },

  q33: {
    type: "wheel_6",
    question: "Which area of Transportation most interests you?",
    answers: [
      {
        a: "Aircraft Pilots",
        next: "END"
      },
      {
        a: "Air Traffic Controllers",
        next: "END"
      },
      {
        a: "Locomotive Engineers",
        next: "END"
      },
      {
        a: "Railroad Conductors",
        next: "END"
      },
      {
        a: "Ship Captains and Marine Pilots",
        next: "END"
      },
      {
        a: "Ship Engineers",
        next: "END"
      }
    ]
  }
};

class signInteractives extends React.Component {

componentDidMount() {
  window.scrollTo(0, 0)
}


  constructor(props) {



    super(props);
    this.state = {

      user_data: [],

      goals_data: [],

      currentRole: null,
      goalRole: null,

      wheel_tile_components: {
        currentQ: opsList1.q1,
        size: "wheel_6",
        second: false
      },

      showhide: {
        ALL: true,
        timeline: false
      }
    };
  }

  myCallback3 = (dataFromChild, dataFromChild2) => {

    if (this.state.wheel_tile_components.second == false) {


        this.setState({ user_data: [...this.state.user_data, dataFromChild] });

        this.setState({currentRole: dataFromChild});



      if (dataFromChild2 != "END") {


        var wheel_tile_components = { ...this.state.wheel_tile_components };

        wheel_tile_components.size = opsList1[dataFromChild2].type.toString();

        wheel_tile_components.currentQ = opsList1[dataFromChild2];
        this.setState({ wheel_tile_components });



      } else if (dataFromChild2 == "END") {

        var wheel_tile_components = { ...this.state.wheel_tile_components };

        wheel_tile_components.size = opsList2.q1.type.toString();

        wheel_tile_components.currentQ = opsList2.q1;

        wheel_tile_components.second = true;

        this.setState({ wheel_tile_components });


      }




    } else {



        this.setState({goalRole: dataFromChild});

      this.setState({ goals_data: [...this.state.goals_data, dataFromChild] });

      if (dataFromChild2 != "END") {
        var wheel_tile_components = { ...this.state.wheel_tile_components };

        wheel_tile_components.size = opsList2[dataFromChild2].type.toString();

        wheel_tile_components.currentQ = opsList2[dataFromChild2.toString()];
        this.setState({ wheel_tile_components });

      } else if (dataFromChild2 == "END") {

        var showhide = { ...this.state.showhide };

        showhide.timeline = true;

        this.setState({ showhide });
      }
    }
  };


  render() {
 
    return (
      <div>

<div>
			<header>
				<p id="title"><a href="/HomePage" style={{float:"left", marginLeft:400, fontSize:45, fontWeight: "bold"}}>My Career Timeline</a>





				
			</p>
			
			<p id="title">
		    	</p>
				
	    	</header>
	    </div>


<br/>
<br/>
<br/>
<br/>


        {this.state.showhide.ALL ? (
          <Allwheels
            ops={this.state.wheel_tile_components.currentQ}
            size={this.state.wheel_tile_components.size}
            callbackFromParent={this.myCallback3}
          />
        ) : null}

<br/>

        {this.state.showhide.timeline ? 
          <Results goal={this.state.goalRole} /> : null}






      </div>
    );
  }
}

export default signInteractives;